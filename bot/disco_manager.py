import asyncio
import aiohttp
from channels_manager import channels_manager


class disco_manager:
    def __init__(self, client, cache_server_addr):
        self.chm=channels_manager(client)
        self.cache_server_addr=cache_server_addr
        self.msgs={}
        self.init=False

    async def loop(self):
        while True:
            channels=self.chm.discover_channels()
            for chname in channels.keys():
                msg=await self.request_msg(chname)
                await self.send_msgs(msg, channels[chname])
            await asyncio.sleep(10)
            self.init=True

    async def request_msg(self, key):
        print ("requesting msg for {}".format(key))
        async with aiohttp.ClientSession() as session:
            async with session.get(self.cache_server_addr+key) as resp:
                return await resp.text()

    async def send_msgs(self, msg, channels):
        if msg:
            for ch in channels:
                if self.init==False:
                    await self.chm.clear_channel(ch)

                if ch in self.msgs:
                    await self.chm.edit_message(self.msgs[ch], msg)
                else:
                    self.msgs[ch]=await self.chm.send_message(ch, msg)
