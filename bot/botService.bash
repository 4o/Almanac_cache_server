#!/bin/bash

mode=$1

if [ "$mode" == "-l" ]; then
    cmd=python3.6
else
    cmd=python
fi

while [ true ]; do
    bash start.bash $1
    if [ "`cat reboot_flag_file.txt`" == "1" ]; then
        echo "bot exited. killing service"
        exit
    fi
    echo "bot exited. restarting in 30 sec"
    sleep 30
done