#!/bin/bash

mode=`uname`

if [ "$mode" == "Linux" ]; then
    cmd=python3.6
else
    cmd=python
fi

$cmd bot.py