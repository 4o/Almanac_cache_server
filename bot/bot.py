import asyncio
import discord
from discord.ext.commands import Bot
from discord.ext import commands
import platform
from disco_manager import disco_manager

reboot_flag_file="reboot_flag_file.txt"

client = Bot(description="placeholder", command_prefix="$", pm_help = False)

file=open(reboot_flag_file, 'w')
file.write("0")
file.close()

LOCAL=True

ip=disco_manager(client, 'http://0.0.0.0:8081/')

@client.event
async def on_ready():
    print('Logged in as '+client.user.name+' (ID:'+client.user.id+') | Connected to '+str(len(client.servers))+' servers | Connected to '+str(len(set(client.get_all_members())))+' users')
    print('--------')
    print('Current Discord.py Version: {} | Current Python Version: {}'.format(discord.__version__, platform.python_version()))
    print('--------')
    print('Use this link to invite {}:'.format(client.user.name))
    print('https://discordapp.com/oauth2/authorize?client_id={}&scope=bot&permissions=8'.format(client.user.id))
    print('--------')
    await ip.loop()

@client.command(pass_context=True)
async def die(ctx):
    file=open(reboot_flag_file, 'w')
    file.write("1")
    file.close()
    await dm.log("bot is going down. see you")
    await client.close()

@client.command(pass_context=True)
async def restart(ctx):
    await dm.log("bot is restarting. see you again in a minute")
    await client.close()

@client.command(pass_context=True)
async def best(ctx, species):
    msg=dm.get_best_in_show(species)
    if len(msg)==0:
        msg="species '"+species+"' not found on any server"
    await client.say(msg)

@client.command(pass_context=True)
async def ping(ctx):
    await client.say("pong")

with open ('token', 'r') as token:
    client.run(token.read())
