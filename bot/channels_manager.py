import asyncio
import discord
from discord.ext.commands import Bot
from discord.ext import commands

LOG_CHANNEL_NAME='log'

class channels_manager:
    def __init__(self, client):
        self.client=client

    def discover_channels(self):
        channels={}
        for server in self.client.servers:
            for channel in server.channels:
                if channel.type == discord.ChannelType.text:
                    if channel.name != LOG_CHANNEL_NAME:
                        if channel.name in channels:
                            channels[channel.name].append(channel)
                        else:
                            channels[channel.name]=[channel]
        return channels

    async def log(self, msg):
        await self.ensure_channels(['log'])
        await asyncio.sleep(1)
        for server in self.client.servers:
            for channel in server.channels:
                if channel.type == discord.ChannelType.text:
                    if channel.name in LOG_CHANNEL_NAME:
                        await self.client.send_message(channel, msg)

    async def ensure_channels(self, names):
        ret=[]
        for name in names:
            for server in self.client.servers:
                channel_found=False
                for channel in server.channels:
                    if channel.name == name:
                        channel_found=True
                        ret.append(channel)

                if channel_found==False:
                    ch=await self.client.create_channel(server, name, type=discord.ChannelType.text)
                    ret.append(ch)
        return ret

    async def delete_messages(self, msg):
        if msg!=None:
            await self.client.delete_message(msg)

    async def send_message(self, channel, txt):
        return await self.client.send_message(channel, txt)

    async def edit_message(self, msg, txt):
        await self.client.edit_message(msg, txt)

    async def clear_channel(self, ch):
        async for msg in self.client.logs_from(ch, 100):
            try:
                await self.client.delete_message(msg)
            except discord.errors.NotFound:
                print ("delete(msg): not found")

