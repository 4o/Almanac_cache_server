import asyncio
import aiohttp
import json


class almanac_client:
    def __init__(self, init_callback):
        self.init_cb=init_callback
        self.url_prefix='http://73.27.212.31:60001/api/'
        self.servers=None
        self.timestamps={}
        self.server_info={}

    async def loop(self):
        while True:
            try:
                await self.update()
                await asyncio.sleep(30)
            except:
                print ("iteration failed")
                await asyncio.sleep(60)

    async def update(self):
        # self.server_list=await self.get_server_list()
        new_info_names=await self.get_new_info_names()
        new_info=await self.download_info(new_info_names)
        self.update_info(new_info)
        # self.init_cb(self.server_list)
        # for server in self.server_list:
        #     self.server_info[server]=await self.get_server_info(server)
        # print("update done")
        # return

    async def get_new_info_names(self):
        ret=[]
        new_timestamps=await self.get_server_timestamp_list()
        for sname in new_timestamps.keys():
            download=False
            if sname in self.timestamps:
                if new_timestamps[sname] != self.timestamps[sname]:
                    download=True
            else:
                self.timestamps[sname]=new_timestamps[sname]
                download=True
            if download:
                ret.append(sname)
                self.timestamps[sname]=new_timestamps[sname]
        return ret

    async def get_server_timestamp_list(self):
        info=await self.get_info()
        ret={}
        servers=info['Servers']
        for server in servers:
            ret[server['Key']]=server['LastUpdate']
        return ret

    async def download_info(self, names):
        ret={}
        for name in names:
            try:
                data=await self.get_server_info(name)
                ret[name]={}
                ret[name]['contents']=data
                ret[name]['timestamp']=self.timestamps[name]
            except:
                print ("failed to fetch "+name)
        return ret

    def update_info(self, info):
        for name in info.keys():
            self.server_info[name]=info[name]['contents']

    def get(self, url_postfix):
        print ("requested: "+url_postfix)
        if url_postfix == "servers":
            if self.servers != None:
                return self.servers
            else:
                print ("servers failure")
                return "{}"
        else:
            if url_postfix in self.server_info:
                return self.server_info[url_postfix]
            else:
                print ("servers_info failure")
                return "{}"

#####################
    async def get_server_list(self):
        info=await self.get_info()
        self.servers=info
        ret=[]
        servers=info['Servers']
        for server in servers:
            ret.append(server['Key'])
        return ret

    async def get_info(self):
        info=await self.download_json(self.url_prefix+"servers")
        self.servers=info
        return info

    async def get_server_info(self, servername):
        return await self.download_json(self.url_prefix+"wildcreatures/"+servername)

    async def download_json(self, url):

        print ("downloading "+url)
        async with aiohttp.ClientSession() as session:
            async with session.get(url) as resp:
                ret=await resp.json()
        print ("done")
        return ret
