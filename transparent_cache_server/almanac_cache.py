import asyncio
from aiohttp import web
import json

routes=web.RouteTableDef()

# @routes.get('/api/servers')
# async def index(request):
#     # name=request.match_info.get('name', "Anonymous")
#     text="Hello\n"
#     return web.Response(text=text)

class almanac_cache:
    def __init__(self, almanac):
        self.routes=routes
        self.almanac=almanac

    def servers(self, request):
        text=json.dumps(self.almanac.get('servers'))
        return web.Response(text=text, headers={'content-type': 'application/json'})

    def island(self, request):
        text=json.dumps(self.almanac.get('island'))
        return web.Response(text=text, headers={'content-type': 'application/json'})

    def ragnarok(self, request):
        text=json.dumps(self.almanac.get('ragnarok'))
        return web.Response(text=text, headers={'content-type': 'application/json'})

    def ragnarokpvp(self, request):
        text=json.dumps(self.almanac.get('ragnarokpvp'))
        return web.Response(text=text, headers={'content-type': 'application/json'})

    def aberration(self, request):
        text=json.dumps(self.almanac.get('aberration'))
        return web.Response(text=text, headers={'content-type': 'application/json'})

    def scorched(self, request):
        text=json.dumps(self.almanac.get('scorched'))
        return web.Response(text=text, headers={'content-type': 'application/json'})

    def loop(self):
        app=web.Application()
        app.router.add_routes(
            [
                web.get('/api/servers', self.servers),
                web.get('/api/wildcreatures/island', self.island),
                web.get('/api/wildcreatures/ragnarok', self.ragnarok),
                web.get('/api/wildcreatures/ragnarokpvp', self.ragnarokpvp),
                web.get('/api/wildcreatures/aberration', self.aberration),
                web.get('/api/wildcreatures/scorched', self.scorched)
            ]
        )
        web.run_app(app)
