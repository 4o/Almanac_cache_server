import sys
import asyncio
from aiohttp import web
import json
from my_cache import my_cache

routes=web.RouteTableDef()
app=web.Application()

local=False
if len(sys.argv)>1:
    if sys.argv[1]=="-l":
        local=True

cache=my_cache(local)

loop=asyncio.get_event_loop()

@routes.get('/{tail:.*}')
async def handle(request):
    if request.method=="GET":
        actual_request=request.path[1:]
        if actual_request=="":
            if request.query_string=="":
                print ("this looks like news without filter request")
                return web.Response(text=cache.get_msg(None))
            else:
                print ("this looks like news with filter request")
                print ("{}".format(request.query_string))
                json_object=json.loads(request.query_string)

                return web.Response(text=cache.get_msg(json_object))
        else:
            print ("got {} request: {}".format(request.method, actual_request))
            return web.Response(text=cache.get_msg(actual_request))
    print ("invalid request")
    return True

app.add_routes(routes)

t1=loop.create_task(cache.loop())
t2=loop.create_task(web.run_app(app, port="8081"))
tasks=[t1, t2]
wait_task=asyncio.wait(tasks)
loop.run_until_complete(wait_task)
loop.close()
