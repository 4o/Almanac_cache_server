import asyncio
import json

from almanac_client import almanac_client

EXCLUDE=[
    'Achatina_Character_BP_C',
    'Salmon_Character_Aberrant_C',
    'RockDrake_Character_BP_C',
    'Pteroteuthis_Char_BP_C',
    'ChupaCabra_Character_BP_C',
    'ChupaCabra_Character_BP_Surface_C',
    'BoaFrill_Character_BP_Aberrant_C',
    'Trilobite_Character_Aberrant_C',
    'Cnidaria_Character_BP_Aberrant_C',
    'Dragonfly_Character_BP_Aberrant_C',
    'Coel_Character_BP_Aberrant_C',
    'Piranha_Character_BP_Aberrant_C',
    'Lamprey_Character_C',
    'Eel_Character_BP_Aberrant_C'
]

LOAD=False
HPF=32

class almanac_interrogator:
    def __init__(self, local):
        self.cli=almanac_client(local)

    async def get_dinos(self):
        data=await self.cli.get_server_info()
        return self.repackage(data)

######################################

    def repackage(self, info):
        ret={}
        for servername in info.keys():
            species=info[servername]['Species']
            ret[servername]=self.repackage_server(species, servername)
        return ret

    def repackage_server(self, server_info, servername):
        ret={}
        for name in server_info.keys():
            if server_info[name]['IsTameable']:
                creatures=server_info[name]['Creatures']
                creatures_new={}
                for c in creatures:
                    max_stat=self.max_stat(c['BaseStats'])
                    if (max_stat >= HPF) and c['IsTameable'] and (name not in EXCLUDE):
                        c['Server']=servername
                        c['Max_stat']=max_stat
                        c['Species']=name
                        c.pop('X')
                        c.pop('Y')
                        c.pop('Z')
                        c.pop('TopoMapX')
                        c.pop('TopoMapY')
                        id=c.pop('Id1')
                        c['BaseStats']=self.clean_stats(c['BaseStats'])
                        creatures_new[id]=c
                ret.update(creatures_new)
        return ret

    def max_stat(self, bs):
        max=0
        for s in bs.keys():
            if max < bs[s]:
                max = bs[s]
        return max

    def clean_stats(self, bs):
        ret={}
        for s in bs:
            if bs[s] >= HPF:
                ret[s]=bs[s]
        return ret
