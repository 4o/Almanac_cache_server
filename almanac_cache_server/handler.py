from msg_helper import compose_server_msg
from msg_helper import compose_species_msg
from msg_helper import compose_news_msg
from msg_helper import is_species


MSG_LEN=1900
LOW_LIMIT=25
HIGH_LIMIT=100
FIFO_LEN=20

class handler:
    def __init__(self, key):
        self.key=key
        self.msg=None
        self.msg_fifo=[]
        self.msg_source="unknown"

    def handle(self, data, updated_servers, news):
        self.msg=self.compose_msg(data, updated_servers, news)

    def compose_msg(self, data, updated_servers, news):
        if not isinstance(self.key, dict):
            if (self.key in data) and (self.key in updated_servers):
                self.msg_source="fixed"
                print ("is server: {}", self.key)
                my_data=data[self.key]

                for limit in range(LOW_LIMIT, HIGH_LIMIT):
                    msg=compose_server_msg(my_data, limit, self.key)
                    if len(msg) < MSG_LEN:
                        return msg

                return "failed to compose msg for server "+self.key

            if is_species(data, self.key):
                self.msg_source="fixed"
                print ("is species: {}", self.key)
                return compose_species_msg(data, self.key)

        if (self.key == None) or (isinstance(self.key, dict) and ('Species' in self.key) and is_species(data, self.key['Species'])):
            print ("is news: {}", self.key)
            self.msg_source="fifo"
            if news:
                new_msg=compose_news_msg(news, self.key)
                if len(self.msg_fifo)>FIFO_LEN:
                    self.msg_fifo.pop(0)
                self.msg_fifo.append(new_msg)
            else:
                return "no news so far"

        return None

    def get_msg(self):
        if self.msg_source == "fixed":
            if self.msg:
                return "``"+self.msg+"``"
            else:
                return "no msg so far1"

        if self.msg_source == "fifo":
            if len(self.msg_fifo)>0:
                return "``"+self.msg_fifo.pop(0)+"``"
            else:
                return "no news so far"

        print ("none msg")
        return None
