import json
from handler import handler


class handler_collection:
    def __init__(self):
        self.handlers={}

    def handle(self, data, keys, news):
        print("running handlers for servers: {}".format(keys))
        for name in self.handlers.keys():
            self.handlers[name].handle(data, keys, news)

    def get_msg(self, data, filter_key):
        dict_key=None
        if filter_key==None:
            dict_key='News'

        if isinstance(filter_key, dict):
            json_string=json.dumps(filter_key, sort_keys=True)
            dict_key=hash(json_string)
        else:
            dict_key=filter_key

        if dict_key not in self.handlers:
            self.handlers[dict_key]=handler(filter_key)
            self.handlers[dict_key].handle(data, data.keys(), None)

        msg=self.handlers[dict_key].get_msg()
        if msg==None:
            print ("removing handler {}".format(filter_key))
            self.handlers.pop(dict_key)
        return msg
