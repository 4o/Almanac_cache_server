def is_species(data, key):
    for servername in data.keys():
        for id in data[servername]:
            if data[servername][id]['Species'].lower()==key:
                return True
    return False

##############################
# server msg

def compose_server_msg(data, limit, servername):
    dinos=get_matching_dinos(data, limit, servername)
    species=get_species(dinos)
    return "server "+servername+"({}):\n".format(limit)+print_species(species, limit)

def get_species(data):
    ret={}
    lut={}
    names=get_species_names(data)
    for name in names:
        (dinos, max_stat)=get_dinos_by_species(data, name)
        ret[name]=dinos
        lut[name]=max_stat
    return (ret, lut)

def get_species_names(data):
    names=[]
    for d in data:
        name=d['Species']
        if name not in names:
            names.append(name)
    return names

def get_dinos_by_species(data, species_name):
    ret=[]
    max_stat=0
    for d in data:
        if d['Species']==species_name:
            ret.append(d)
            if max_stat < d['Max_stat']:
                max_stat=d['Max_stat']
    return (ret, max_stat)

def print_species(data, limit):
    species=data[0]
    lut=data[1]
    msg=""
    while len(lut) > 0:
        name = pop_max_stat_species(lut)
        dinos=print_dinos(species[name], limit)
        if len(dinos) > 0:
            msg+="    "+name+":\n"
            msg+=dinos
    return msg

def pop_max_stat_species (lut):
    ret=None
    max_stat=0
    for name in lut.keys():
        if max_stat <= lut[name]:
            ret=name
            max_stat = lut[name]
    lut.pop(ret)
    return ret

def get_matching_dinos(data, limit, servername):
    ret=[]
    for id in data.keys():
        if (data[id]['Max_stat']>=limit) and data[id]['Server']==servername:
            ret.append(data[id])
    return ret

def print_dinos(dinos, limit):
    msg=""
    for d in dinos:
        msg+=print_dino(d, limit)
    return msg

def print_dino(dino, stat_cap):
    if dino['Max_stat']>=stat_cap:
        return "        "+print_gender(dino)+" "+format_stats(dino, stat_cap)+print_pos(dino)+print_lvl(dino)+"\n"
    else:
        return ""

#######################################
# species msg

def compose_species_msg(data, species_name):
    best=None
    for servername in data.keys():
        best_on_server=get_best_dinos_for_species(get_dinos_by_species_from_dict(data[servername], species_name))
        best=merge_best_dinos(best, best_on_server)
    return compose_best_msg(best)

def compose_best_msg(best):
    msg=""
    for key in best.keys():
        msg+=compose_best_msg_for_dino(best[key], key)
    return msg

def compose_best_msg_for_dino(dino, stat, offset=""):
    return offset+print_gender(dino)+" "+format_stats(dino, stat_cap=None, statname=stat)+print_pos(dino)+print_lvl(dino)+" @"+dino['Server']+"\n"

def get_best_dinos_for_species(dinos):
    ret={}
    for d in dinos[0]:
        for statname in d['BaseStats'].keys():
            if statname not in ret:
                ret[statname]=d
            else:
                if ret[statname]['BaseStats'][statname]<d['BaseStats'][statname]:
                    ret[statname]=d
    return ret

def merge_best_dinos(best, best_on_server):
    if best == None:
        return best_on_server

    for statname in best_on_server.keys():
        if statname in best:
            if best_on_server[statname]['BaseStats'][statname] > best[statname]['BaseStats'][statname]:
                best[statname]=best_on_server[statname]
        else:
            best[statname]=best_on_server[statname]
    return best

def get_dinos_by_species_from_dict(data, species_name):
    ret=[]
    max_stat=0
    for dn in data.keys():
        d=data[dn]
        if d['Species'].lower()==species_name:
            ret.append(d)
            if max_stat < d['Max_stat']:
                max_stat=d['Max_stat']
    return (ret, max_stat)

#######################################
# news msg

def compose_news_msg(data, config):
    msg=""
    for name in data.keys():
        new_msg=print_dino_list(data[name]['New'], config)
        removed_msg=print_dino_list(data[name]['Removed'], config)
        if new_msg or removed_msg:
            msg+=name
        if new_msg:
            msg+=name+"\n  new:\n"+new_msg
        if removed_msg:
            msg+="  removed:\n"+removed_msg
    return msg

def print_dino_list(data, config):
    msg=""
    filtered_data=filter_dinos(data, config)
    sorted_data=sorted(filtered_data, key=lambda k: k['Max_stat'], reverse=True)
    for d in sorted_data:
        msg+=compose_best_msg_for_dino_news(d)
    return msg

def filter_dinos(data, config):
    if config==None:
        return data
    list_to_remove=[]
    for d in data:
        remove=True
        remove=filter_news_dinos(d, config)

        if remove:
            list_to_remove.append(d)

    for item in list_to_remove:
        data.remove(item)
    return data

def filter_news_dinos(dino, cfg):
    if dino['Species'].lower()==cfg['Species']:
        if 'Filter' in cfg:
            if check_dino(dino, cfg['Filter']) == False:
                return False
        else:
            return False
    return True

def check_dino(dino, cfg):
    for cfg_record in cfg:
        if check_dino_stats_loop(dino, cfg_record) == False:
            return False
    return True

def check_dino_stats_loop(d, cfg_record):
    stats=cfg_record['Stats']
    threshold=cfg_record['Threshold']
    base_stats=d['BaseStats']
    for bsn in base_stats.keys():
        do_threshold_check=True
        if stats:
            if bsn[:2].lower() not in stats:
                do_threshold_check=False
        if do_threshold_check:
            if base_stats[bsn]>=threshold:
                return False
    return True

def compose_best_msg_for_dino_news(dino):
    return "    "+print_gender(dino)+" "+dino['Species']+" "+format_stats(dino, 0)+print_pos(dino)+print_lvl(dino)+"\n"

#######################################
# common

def print_gender(creature):
    return creature['Gender'][:1]

def print_lvl(creature):
    return " lvl: {0}".format(creature['BaseLevel'])

def format_stats(creature, stat_cap, statname=None):
    msg=""
    if statname:
        msg+=statname[:2]+": {0}".format(creature['BaseStats'][statname])+" "
    else:
        stats=creature['BaseStats']
        for stat in stats.keys():
            if stat_cap:
                if stats[stat] >= stat_cap:
                    msg+=stat[:2]+": {0}".format(stats[stat])+" "
            else:
                msg+=stat[:2]+": {0}".format(stats[stat])+" "
    return msg

def print_pos(creature):
    return "pos: {0:.1f} {1:.1f}".format(creature['Latitude'], creature['Longitude'])
