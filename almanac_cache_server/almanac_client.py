import asyncio
import aiohttp
import json

class almanac_client:
    def __init__(self, local):
        self.url_prefix=None
        if local:
            self.url_prefix='http://0.0.0.0:8080/api/'
        else:
            self.url_prefix='http://73.27.212.31:60001/api/'
        self.servers={}

    def get_server_list(self, info):
        ret=[]
        servers=info['Servers']
        for server in servers:
            ret.append(server['Key'])
        return ret

    async def get_info(self):
        info=await self.download_json(self.url_prefix+"servers")
        to_update=self.get_updated_servers(info)
        return to_update

    def get_updated_servers(self, info):
        ret=[]
        timestamps=self.get_timestamps(info)
        for name in timestamps.keys():
            if name not in self.servers:
                self.servers[name]=None

            if self.servers[name]!=timestamps[name]:
                self.servers[name]=timestamps[name]
                ret.append(name)
        return ret

    def get_timestamps(self, info):
        ret={}
        servers=info['Servers']
        for server in servers:
            ret[server['Key']]=server['LastUpdate']
        return ret

    async def get_server_info(self, servername=None):
        if servername==None:
            to_update=await self.get_info()
            ret={}
            for name in to_update:
                ret[name]=await self.download_json(self.url_prefix+"wildcreatures/"+name)
            return ret
        else:
            return await self.download_json(self.url_prefix+"wildcreatures/"+servername)

    async def download_json(self, url):

        async with aiohttp.ClientSession() as session:
            async with session.get(url) as resp:
                ret=await resp.json()
        return ret
