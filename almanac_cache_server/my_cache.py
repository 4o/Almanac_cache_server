import asyncio
import json
from almanac_interrogator import almanac_interrogator
from handler_collection import handler_collection


BACKUP_FILE="/tmp/almanac_cache_backup_file"
POLL_PERIOD=5

class my_cache:
    def __init__(self, local):
        self.int=almanac_interrogator(local)
        self.data={}
        self.handlers=handler_collection()
        self.load_data()

    async def loop(self):
        while True:
            new_data=await self.int.get_dinos()
            keys=new_data.keys()
            if keys:
                news=self.update_data(new_data)
                self.handlers.handle(self.data, keys, news)
                self.dump_data()
            await asyncio.sleep(POLL_PERIOD)

#######################
# ext iface

    def get_msg(self, filter_key):
        return self.handlers.get_msg(self.data, filter_key)

#######################

    def update_data(self, new_data):
        news={}
        for name in new_data.keys():
            if name not in self.data:
                self.data[name]={}
            news[name]=self.get_news(new_data[name], self.data[name])
            self.data[name]=new_data[name]
        return news

    def get_news(self, new_data, old_data):
        ret = {}
        ret['New']=[]
        ret['Removed']=[]

        new_dinos=set(new_data.keys()) - set(old_data.keys())
        removed_dinos=set(old_data.keys()) - set(new_data.keys())

        for n in new_dinos:
            d=new_data[n]
            ret['New'].append(d)

        for n in removed_dinos:
            d=old_data[n]
            ret['Removed'].append(d)

        return ret

    def dump_data(self, obj=None, filename=None):
        if obj==None:
            obj=self.data
        if filename==None:
            filename=BACKUP_FILE
        with open(filename, 'w') as f:
            json.dump(obj, f)
        print ("backup ok")

    def load_data(self):
        try:
            with open(BACKUP_FILE, 'r') as f:
                data=json.load(f)
            for s in data.keys():
                self.data[s]={}
                for n in data[s]:
                    self.data[s][int(float(n))]=data[s][n]
            print ("backup file loaded")
        except:
            print ("no backup file found")
